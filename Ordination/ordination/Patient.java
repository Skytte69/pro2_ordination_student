package ordination;

import java.util.HashSet;
import java.util.Set;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private Set<Ordination> ordinationer = new HashSet<Ordination>();

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	public Set<Ordination> getOrdinationer() {
		return new HashSet<Ordination>(ordinationer);
	}

	public Ordination addOrdination(Ordination ordination) {
		ordinationer.add(ordination);
		return ordination;
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

}
