package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

public class PN extends Ordination {
	private TreeMap<LocalDate, Integer> ordinationsDage = new TreeMap<LocalDate, Integer>();
	private double antalEnheder;

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean added = false;

		// Hvis givesDen ligger mellem eller på start og slut dag
		if (givesDen.compareTo(getStartDen()) >= 0 && givesDen.compareTo(getSlutDen()) <= 0) {
			ordinationsDage.put(givesDen, ordinationsDage.getOrDefault(givesDen, 0) + 1);
			added = true;
		}
		return added;
	}

	public double doegnDosis() {
		double dosis = 0;

		if (getAntalGangeGivet() > 0) {
			long antalDage = ChronoUnit.DAYS.between(ordinationsDage.firstKey(), ordinationsDage.lastKey()) + 1;
			dosis = (getAntalGangeGivet() * antalEnheder) / antalDage;
		}

		return dosis;
	}

	public double samletDosis() {
		return getAntalGangeGivet() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		int antal = 0;
		for (Map.Entry<LocalDate, Integer> od : ordinationsDage.entrySet()) {
			antal += od.getValue();
		}
		return antal;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
