package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public void opretDosis(LocalTime tid, double antal) {
		doser.add(new Dosis(tid, antal));
	}

	@Override
	public double samletDosis() {
		double dosis = 0;
		// Medregner dagen medicinen er ordineret
		long days = ChronoUnit.DAYS.between(this.getStartDen(), this.getSlutDen()) + 1;
		dosis = days * doegnDosis();
		return dosis;
	}

	@Override
	public double doegnDosis() {
		double doseAntal = 0;
		for (Dosis dosis : doser) {
			doseAntal += dosis.getAntal();
		}
		return doseAntal;
	}

	@Override
	public String getType() {
		return "Daglig skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}


}
