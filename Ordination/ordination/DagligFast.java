package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	@Override
	public double samletDosis() {
		double dosis = 0;
		long days = ChronoUnit.DAYS.between(this.getStartDen(), this.getSlutDen()) + 1;
		dosis = days * doegnDosis();
		return dosis;
	}

	public void setDoser(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		doser[0] = new Dosis(LocalTime.of(7, 00), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(22, 00), natAntal);
	}

	@Override
	public double doegnDosis() {
		double doseAntal = 0;
		for (Dosis dosis : doser) {
			doseAntal += dosis.getAntal();
		}
		return doseAntal;
	}

	public Dosis[] getDoser() {
		return doser.clone();
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}
}
