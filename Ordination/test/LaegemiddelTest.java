package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import ordination.Laegemiddel;

public class LaegemiddelTest {
	
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		laegemiddel = new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

	}

	@Test
	public void testGetEnhedPrKgPrDoegnLet() {
		assertEquals(laegemiddel.getEnhedPrKgPrDoegnLet(), 0.01, 0.01);
	}

	@Test
	public void testGetEnhedPrKgPrDoegnNormal() {
		assertEquals(laegemiddel.getEnhedPrKgPrDoegnNormal(), 0.15, 0.15);
	}

	@Test
	public void testGetEnhedPrKgPrDoegnTung() {
		assertEquals(laegemiddel.getEnhedPrKgPrDoegnTung(), 0.2, 0.2);
	}


}
