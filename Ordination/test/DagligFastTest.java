package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;

public class DagligFastTest {
	private LocalDate startDen;
	private LocalDate slutDen;
	private DagligFast df;

	@Before
	public void setUp() throws Exception {
		startDen = LocalDate.of(2020, 02, 21);
		slutDen = LocalDate.of(2020, 02, 22);
		df = new DagligFast(startDen, slutDen);

	}

	// Tester typen
	@Test
	public void testGetType() {
		assertEquals(df.getType(), "Daglig fast");
	}

	// Tester starttidspunkt
	@Test
	public void testGetStartDen() {
		assertEquals(df.getStartDen(), startDen);
	}

	// Tester sluttidspunkt
	@Test
	public void testGetSlutDen() {
		assertEquals(df.getSlutDen(), slutDen);
	}

	// setDoser() + getDoser()
	@Test
	public void testGetSetDagligFast1111Dose() {
		df.setDoser(1, 1, 1, 1);
		Dosis[] doser = new Dosis[4];
		doser[0] = new Dosis(LocalTime.of(7, 00), 1.0);
		doser[1] = new Dosis(LocalTime.of(12, 00), 1.0);
		doser[2] = new Dosis(LocalTime.of(18, 00), 1.0);
		doser[3] = new Dosis(LocalTime.of(22, 00), 1.0);
		assertArrayEquals(df.getDoser(), doser);
	}

	// setDoser() + doegnDosis()
	@Test
	public void testGetDoegnDosis4() {
		df.setDoser(1, 1, 1, 1);
		assertEquals(df.doegnDosis(), 4, 0.001);
	}

	// setDoser() + doegnDosis()
	@Test
	public void testGetDoegnDosis5() {
		df.setDoser(1, 2, 1, 1);
		assertEquals(df.doegnDosis(), 5, 0.001);
	}

	// setDoser() + samletDosis()
	@Test
	public void getSamletDosis2Dage4DagligeDoser() {
		df.setDoser(1, 1, 1, 1);
		assertEquals(df.samletDosis(), 8, 0.001);
	}

	// setDoser() + samletDosis()
	@Test
	public void getSamletDosis2Dage5DagligeDoser() {
		df.setDoser(1, 2, 1, 1);
		assertEquals(df.samletDosis(), 10, 0.001);
	}

}
