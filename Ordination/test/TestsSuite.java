package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ControllerTest.class, DagligFastTest.class, DagligSkaevTest.class, DosisTest.class,
		LaegemiddelTest.class, PatientTest.class, PNTest.class })

public class TestsSuite {

}
