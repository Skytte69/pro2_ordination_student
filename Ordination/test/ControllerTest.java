package test;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Controller controller;
	private LocalDate startDate, date1;
	private double antal;
	private Laegemiddel laegemiddel;

	private Patient patientLille;
	private Patient patientMellem;
	private Patient patientStor;

	private PN pn;

	private LocalTime[] klokkeSlet1 = new LocalTime[4];
	private double antalEnheder1[] = new double[4];

	private LocalTime[] klokkeSlet2 = new LocalTime[4];
	private double antalEnheder2[] = new double[4];

	private double antalEnheder3[] = new double[4];

	@Before
	public void setUp() throws Exception {
		startDate = LocalDate.of(2020, 02, 20);
		date1 = LocalDate.of(2020, 02, 21);

		antal = 2;

		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		pn = new PN(startDate, date1, 1.0);

		patientLille = new Patient("121256-0512", "Jane Jensen", 20.4);
		patientMellem = new Patient("121256-0512", "Hanne Jensen", 65.4);
		patientStor = new Patient("121256-0512", "Lone Jensen", 130.4);

		controller = Controller.getController();

		klokkeSlet1[0] = LocalTime.of(18, 00);
		antalEnheder1[0] = 2;

		klokkeSlet2[0] = LocalTime.of(10, 00);
		klokkeSlet2[1] = LocalTime.of(18, 00);
		klokkeSlet2[2] = LocalTime.of(22, 00);

		antalEnheder2[0] = 2;
		antalEnheder2[1] = 2;
		antalEnheder2[2] = 3;

		antalEnheder3[0] = 2;
		antalEnheder3[1] = -1;
	}

	@Test
	public void testOpretPNOrdination1() {
		Assert.assertNotNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille, laegemiddel, antal));
	}

	@Test
	public void testOpretPNOrdination2() {
		Assert.assertNotNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 20), patientLille, laegemiddel, antal));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdination3() {
		Assert.assertNotNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 19), patientLille, laegemiddel, antal));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretPNOrdination4() {
		Assert.assertNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille, null, antal));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretPNOrdination5() {
		Assert.assertNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 21), null, laegemiddel, antal));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdination6() {
		Assert.assertNull(
				controller.opretPNOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille, laegemiddel, -1));
	}

	// OPRET DAGLIGFASTORDINATION

	@Test
	public void testOpretDagligFastOrdination1() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, 0, 0, 0, 2));
	}

	@Test
	public void testOpretDagligFastOrdination2() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, 0, 0, 2, 2));
	}

	@Test
	public void testOpretDagligFastOrdination3() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, 0, 2, 2, 2));
	}

	@Test
	public void testOpretDagligFastOrdination4() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, 2, 2, 2, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination5() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 19), patientLille,
				laegemiddel, 2, 2, 2, 2));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretDagligFastOrdination6() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), null,
				laegemiddel, 2, 2, 2, 2));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretDagligFastOrdination7() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				null, 2, 2, 2, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination8() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, -1, 2, 2, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination9() {
		Assert.assertNotNull(controller.opretDagligFastOrdination(startDate, LocalDate.of(2020, 02, 22), patientLille,
				laegemiddel, 0, 0, 0, 0));
	}

	@Test
	public void testOpretDagligSkaevOrdination1() {
		Assert.assertNotNull(controller.opretDagligSkaevOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille,
				laegemiddel, klokkeSlet1, antalEnheder1));
	}

	@Test
	public void testOpretDagligSkaevOrdination2() {
		Assert.assertNotNull(controller.opretDagligSkaevOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille,
				laegemiddel, klokkeSlet2, antalEnheder2));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretDagligSkaevOrdination3() {
		Assert.assertNotNull(controller.opretDagligSkaevOrdination(startDate, LocalDate.of(2020, 02, 21), null,
				laegemiddel, klokkeSlet1, antalEnheder1));
	}

	@Test(expected = NullPointerException.class)
	public void testOpretDagligSkaevOrdination4() {
		Assert.assertNotNull(controller.opretDagligSkaevOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille,
				null, klokkeSlet1, antalEnheder1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination5() {
		Assert.assertNotNull(controller.opretDagligSkaevOrdination(startDate, LocalDate.of(2020, 02, 21), patientLille,
				laegemiddel, klokkeSlet2, antalEnheder3));
	}

	@Test
	public void testOrdinationPNAnvendt1() {
		controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 20));
		Assert.assertEquals(pn.getAntalEnheder(), 1, 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn1() {
		Assert.assertEquals(controller.anbefaletDosisPrDoegn(patientLille, laegemiddel), 2.04, 00.1);
	}

	@Test
	public void testAnbefaletDosisPrDoegn2() {
		Assert.assertEquals(controller.anbefaletDosisPrDoegn(patientMellem, laegemiddel), 9.81, 00.1);
	}

	@Test
	public void testAnbefaletDosisPrDoegn3() {
		Assert.assertEquals(controller.anbefaletDosisPrDoegn(patientStor, laegemiddel), 20.8, 00.1);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel1() {
		Assert.assertNotNull(controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, laegemiddel));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel2() {
		Assert.assertNotNull(controller.antalOrdinationerPrVægtPrLægemiddel(0, 50, laegemiddel));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel3() {
		Assert.assertNotNull(controller.antalOrdinationerPrVægtPrLægemiddel(-1, 50, laegemiddel));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel4() {
		Assert.assertNotNull(controller.antalOrdinationerPrVægtPrLægemiddel(-1, 50, null));
	}
}
