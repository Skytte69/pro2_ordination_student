package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.PN;

public class PNTest {
	private PN pn;
	private LocalDate startDen, slutDen, slutDen2;

	@Before
	public void setUp() throws Exception {
		startDen = LocalDate.of(2020, 02, 21);
		slutDen = LocalDate.of(2020, 02, 22);
		slutDen2 = LocalDate.of(2020, 02, 23);
	}

	// Tester typen
	@Test
	public void testGetType() {
		pn = new PN(startDen, slutDen, 1.5);
		assertEquals(pn.getType(), "PN");
	}

	// test af antal enheder
	@Test
	public void testGetAntalEnheder() {
		pn = new PN(startDen, slutDen, 1.5);
		assertEquals(1.5, pn.getAntalEnheder(), 0.001);
	}

	// Tester starttidspunkt
	@Test
	public void testGetStartDen() {
		pn = new PN(startDen, slutDen, 1.5);
		assertEquals(pn.getStartDen(), startDen);
	}

	// Tester sluttidspunkt
	@Test
	public void testGetSlutDen() {
		pn = new PN(startDen, slutDen, 1.5);
		assertEquals(pn.getSlutDen(), slutDen);
	}

	// test af givDosis() gives en dato, som ligger før start datoen
	@Test
	public void testGivDosis1() {
		pn = new PN(startDen, slutDen, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 02, 20)));
	}

	// test af givDosis() gives start dato
	@Test
	public void testGivDosis2() {
		pn = new PN(startDen, slutDen, 1);
		assertEquals(true, pn.givDosis(startDen));
	}

	// test af givDosis() gives slut dato
	@Test
	public void testGivDosis3() {
		pn = new PN(startDen, slutDen, 1);
		assertEquals(true, pn.givDosis(slutDen));
	}

	// test af givDosis() gives en dato, som ligger efter slut datoen
	@Test
	public void testGivDosis4() {
		pn = new PN(startDen, slutDen, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 02, 23)));
	}

	// tester 0 gange givet
	@Test
	public void testGetAntalGangeGivet1() {
		pn = new PN(startDen, slutDen, 1);
		assertEquals(0, pn.getAntalGangeGivet());
	}

	// tester 1 gang givet
	@Test
	public void testGetAntalGangeGivet2() {
		pn = new PN(startDen, slutDen, 1);
		pn.givDosis(startDen);
		assertEquals(1, pn.getAntalGangeGivet());
	}

	// tester 2 gange givet
	@Test
	public void testGetAntalGangeGivet3() {
		pn = new PN(startDen, slutDen, 1);
		pn.givDosis(startDen);
		pn.givDosis(slutDen);
		assertEquals(2, pn.getAntalGangeGivet());
	}

	// tester 2 gange på en dag givet
	@Test
	public void testGetAntalGangeGivet4() {
		pn = new PN(startDen, slutDen, 1);
		pn.givDosis(startDen);
		pn.givDosis(startDen);
		assertEquals(2, pn.getAntalGangeGivet());
	}

	// tester samlet dosis på givet dose 1 gang
	@Test
	public void testSamletDosis1() {
		pn = new PN(startDen, slutDen, 1.5);
		pn.givDosis(startDen);
		assertEquals(1.5, pn.samletDosis(), 0.001);
	}

	// tester samlet dosis på givet dose 2 gange
	@Test
	public void testSamletDosis2() {
		pn = new PN(startDen, slutDen, 1.5);
		pn.givDosis(startDen);
		pn.givDosis(slutDen);
		assertEquals(3, pn.samletDosis(), 0.001);
	}

	// tester samlet dosis på givet dose 2 gange samme dag
	@Test
	public void testSamletDosis3() {
		pn = new PN(startDen, slutDen, 1.5);
		pn.givDosis(startDen);
		pn.givDosis(startDen);
		assertEquals(3, pn.samletDosis(), 0.001);
	}

	// tester døgn dosis givet 1 gang på en dag
	@Test
	public void testDoegnDosis1() {
		pn = new PN(startDen, slutDen, 1);
		pn.givDosis(startDen);
		assertEquals(1, pn.doegnDosis(), 0.001);
	}

	// tester døgn dosis givet 2 gange på en dag
	@Test
	public void testDoegnDosis2() {
		pn = new PN(startDen, slutDen, 1);
		pn.givDosis(startDen);
		pn.givDosis(startDen);
		assertEquals(2, pn.doegnDosis(), 0.001);
	}

	// tester døgn dosis givet 2 gange på 3 dage
	@Test
	public void testDoegnDosis3() {
		pn = new PN(startDen, slutDen2, 1);
		pn.givDosis(startDen);
		pn.givDosis(slutDen2);
		assertEquals(0.666, pn.doegnDosis(), 0.001);
	}

}
