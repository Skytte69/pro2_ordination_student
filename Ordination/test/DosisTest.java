package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.Dosis;

public class DosisTest {
	private Dosis d;
	private LocalTime time;
	private double antal;

	@Before
	public void setUp() throws Exception {
		time = LocalTime.of(18, 00);
		antal = 1.0;
		d = new Dosis(time, antal);
	}

	@Test
	public void testGetAntal() {
		assertEquals(d.getAntal(), antal, 0.001);
	}

	@Test
	public void testGetTid() {
		assertEquals(d.getTid(), time);
	}

}
