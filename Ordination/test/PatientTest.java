package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Ordination;
import ordination.Patient;

public class PatientTest {
	private Patient p;
	private String cprNr;
	private String name;
	private double vaegt;
	private Set<Ordination> o = new HashSet<Ordination>();

	@Before
	public void setUp() throws Exception {
		cprNr = "210919901234";
		name = "Maibritt";
		vaegt = 50.0;

		p = new Patient(cprNr, name, vaegt);
		o.add(new DagligSkaev(LocalDate.of(2020, 02, 21), LocalDate.of(2020, 02, 22)));
		p.addOrdination(new DagligSkaev(LocalDate.of(2020, 02, 21), LocalDate.of(2020, 02, 22)));

	}

	@Test
	public void testGetCprnr() {
		assertEquals(p.getCprnr(), "210919901234");
	}

	@Test
	public void testGetNavn() {
		assertEquals(p.getNavn(), "Maibritt");
	}

	@Test
	public void testGetVaegt() {
		assertEquals(p.getVaegt(), 50.0, 0.001);
	}

	@Test
	public void testGetOrdinationer() {
		boolean equals = true;
		if (o.size() != p.getOrdinationer().size()) {
			equals = false;
		}
		assertEquals(true, equals);
	}

}
