package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;

public class DagligSkaevTest {
	private LocalDate startDen, slutDen;
	private LocalTime tid, tid2;
	private DagligSkaev dagligSkæv;
	private ArrayList<Dosis> expekted = new ArrayList<>();

	@Before
	public void setUp() throws Exception {
		startDen = LocalDate.of(2020, 02, 21);
		slutDen = LocalDate.of(2020, 02, 22);
		tid = LocalTime.of(18, 00);
		tid2 = LocalTime.of(18, 30);

	}

	// Tester typen
	@Test
	public void testGetType() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(dagligSkæv.getType(), "Daglig skæv");
	}

	// Tester starttidspunkt
	@Test
	public void testGetStartDen() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(dagligSkæv.getStartDen(), startDen);
	}

	// Tester sluttidspunkt
	@Test
	public void testGetSlutDen() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(dagligSkæv.getSlutDen(), slutDen);
	}

	// tester getDoser for 1 dose
	@Test
	public void testGetDoser1() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		dagligSkæv.opretDosis(tid, 1);
		expekted.add(new Dosis(tid, 1));
		assertEquals(dagligSkæv.getDoser(), expekted);
	}

	// tester getDoser for 0
	@Test
	public void testGetDoser2() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(dagligSkæv.getDoser(), expekted);
	}

	// tester af døgn dosis (+OpretDosis()) med 0 doser
	@Test
	public void testDoegnDosis1() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(0, dagligSkæv.doegnDosis(), 0.001);
	}

	// tester af døgn dosis (+OpretDosis()) med 1 dose
	@Test
	public void testDoegnDosis2() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		dagligSkæv.opretDosis(tid, 1);
		expekted.add(new Dosis(tid, 1));
		assertEquals(1, dagligSkæv.doegnDosis(), 0.001);
	}

	// tester af døgn dosis (+OpretDosis()) med 2 doser
	@Test
	public void testDoegnDosis3() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		dagligSkæv.opretDosis(tid, 1);
		dagligSkæv.opretDosis(tid2, 1);
		expekted.add(new Dosis(tid, 1));
		expekted.add(new Dosis(tid2, 1));
		assertEquals(2, dagligSkæv.doegnDosis(), 0.001);
	}

	// test af samlet dosis med 0 doser
	@Test
	public void testSamletDosis1() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		assertEquals(0, dagligSkæv.samletDosis(), 0.001);
	}

	// test af samlet dosis med 1 dose
	@Test
	public void testSamletDosis2() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		dagligSkæv.opretDosis(tid, 1);
		expekted.add(new Dosis(tid, 1));
		assertEquals(2, dagligSkæv.samletDosis(), 0.001);
	}

	// test af samlet dosis med 2 doser
	@Test
	public void testSamletDosis3() {
		dagligSkæv = new DagligSkaev(startDen, slutDen);
		dagligSkæv.opretDosis(tid, 1);
		dagligSkæv.opretDosis(tid2, 1);
		expekted.add(new Dosis(tid, 1));
		expekted.add(new Dosis(tid2, 1));
		assertEquals(4, dagligSkæv.samletDosis(), 0.001);
	}
}
